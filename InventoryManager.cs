﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace InventoryManager_Prototype
{
    class InventoryManager
    {
        //===============================//
        //=======Private=Variables=======//
        //===============================//
        private static List<InventoryItem> inventoryItemList = new List<InventoryItem>();

        //==============================//
        //=======addInventoryItem=======//
        //==============================//
        public void addInventoryItem(InventoryItem newItem)
        {
            inventoryItemList.Add(newItem);
        }

        //=================================//
        //=======removeInventoryItem=======//
        //=================================//
        public void removeInventoryItem(InventoryItem removedItem)
        {
            inventoryItemList.Remove(removedItem);
        }

        //==================================//
        //=======restockInventoryItem=======//
        //==================================//
        public void restockInventoryItem(InventoryItem item, int numberOfNew)
        {
            InventoryItem itemInList = findSpecificItem(item);  //Finding item in list
            itemInList.addItemStock(numberOfNew);  //Updating entry
        }

        //======================================//
        //=======displayAllInventoryItems=======//
        //======================================//
        public void displayAllInventoryItems()
        {
            for (int currentItem = 0; currentItem < inventoryItemList.Count; currentItem++)  //Looping through list of items
            {
                InventoryItem ci = inventoryItemList[currentItem];  //Defining current item for ease of use
                Console.WriteLine("-> " + ci.getItemName() + " for $" + ci.getItemPrice() + " with " + ci.getItemStock() + " left in stock: " + ci.getItemDescription());  //Printing inventory item
            }
        }

        //===========================//
        //=======searchForItem=======//
        //===========================//
        public List<InventoryItem> searchForItem(String nameSearch, int amountSearch)
        {
            List<InventoryItem> searchHits = new List<InventoryItem>();  //Creating a search hit list

            //======Searching======//
            //===If=Name=Is=Searched===//
            if (nameSearch != null)  //If nameSearch not empty
            {
                for (int currentItem = 0; currentItem < inventoryItemList.Count; currentItem++)  //Looping through list of items
                {
                    if (inventoryItemList[currentItem].getItemName().ToLower().CompareTo(nameSearch.ToLower()) == 0)  //Checking if names are the same
                    {
                        searchHits.Add(inventoryItemList[currentItem]);  //Adding hit to list
                    }
                }
            }
            //===If=Amount=Is=Searched===//
            else if (amountSearch != 0)
            {
                for (int currentItem = 0; currentItem < inventoryItemList.Count; currentItem++)  //Looping through list of items
                {
                    if (inventoryItemList[currentItem].getItemStock() == amountSearch)  //Checking if stock counts are the same
                    {
                        searchHits.Add(inventoryItemList[currentItem]);  //Adding hit to list
                    }
                }
            }

            //======Returning======//
            //===If=There=Are=No=Hits===//
            if (searchHits.Count == 0)
            {
                return null;  //Returning null, because nothing found
            }
            //===If=There=Are=Hits===//
            else
            {
                return searchHits;  //Returning the list of hits
            } 
        }

        //==============================//
        //=======findSpecificItem=======//
        //==============================//
        private InventoryItem findSpecificItem(InventoryItem itemSearch)
        {
            //======If=Item=Is=Searched======//
            if (itemSearch != null)  //If itemSearch not empty
            {
                for (int currentItem = 0; currentItem < inventoryItemList.Count; currentItem++)  //Looping through list of items
                {
                    //===Checking=If=The=Item=Is=Exactly=The=Same===//
                    //This is an attempt to stop mix ups with duplicated items//
                    if (inventoryItemList[currentItem].getItemName().CompareTo(itemSearch.getItemName()) == 0 &&  //Checking if the names are the same
                        inventoryItemList[currentItem].getItemDescription().CompareTo(itemSearch.getItemDescription()) == 0 &&  //Checking if descriptions are the same
                        inventoryItemList[currentItem].getItemPrice() == itemSearch.getItemPrice() &&  //Checking if prices are the same
                        inventoryItemList[currentItem].getItemSize() == itemSearch.getItemSize() &&  //Checking if sizes are the same
                        inventoryItemList[currentItem].getItemStock() == itemSearch.getItemStock() &&  //Checking if stocks are same
                        inventoryItemList[currentItem].getItemWeight() == itemSearch.getItemWeight())  //Checking if weights are the same
                    {
                        return inventoryItemList[currentItem];  //Returning match
                    }
                }
                return null;  //Nothing found, so returning a null
            }
            else
            {
                return null;  //Nothing entered, so returning a null
            }
        }
    }
}
