﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace InventoryManager_Prototype
{
    class Program
    {
        static InventoryManager inventory = new InventoryManager();

        //==================//
        //=======Main=======//
        //==================//
        static void Main(string[] args)
        {
            //======Test=Items======//
            InventoryItem testItem1 = new InventoryItem("Headset", 20.50, 5, 0, 0, "Standard headset");  //Creating new item
            inventory.addInventoryItem(testItem1);  //Adding to item inventory
            InventoryItem testItem2 = new InventoryItem("Keyboard", 15.99, 5, 0, 0, "Standard keyboard");  //Creating new item
            inventory.addInventoryItem(testItem2);  //Adding to item inventory
            InventoryItem testItem3 = new InventoryItem("Headset Pro", 49.8, 5, 0, 0, "Pro headset");  //Creating new item
            inventory.addInventoryItem(testItem3);  //Adding to item inventory

            //======-======//
            Console.WriteLine("CST-117 Milestone Project - Inventory Manager");
            Console.WriteLine("");
            textmenu_Main();  //Starting text menu
        }

        //===========================//
        //=======textmenu_Main=======//
        //===========================//
        static void textmenu_Main()
        {
            //======Instantiating======//
            Boolean runningMenu = true;  //Boolean for whether the menu is running
            while (runningMenu == true)  //Keeping the menu running
            {
                //======Printing======//
                Console.WriteLine("<======Main=Menu======>");
                Console.WriteLine("1) Create New Item");
                Console.WriteLine("2) Remove Item");
                Console.WriteLine("3) Restock Item");
                Console.WriteLine("4) Display All Items");
                Console.WriteLine("5) Search for an item");
                Console.WriteLine();
                Console.Write("Enter a Command: ");

                //======Receiving=Input======//
                String userinput_Raw = Console.ReadLine();
                String userinput_Translated = userinput_Raw.ToLower();
                Console.WriteLine();

                switch (userinput_Translated)
                {
                    //===Create=New=Item===//
                    case "1":
                    case "create":
                    case "new":
                        textmenu_NewItem();
                        break;
                    //===Remove=Item===//
                    case "2":
                    case "remove":
                    case "delete":
                        textmenu_RemoveItem();
                        break;
                    //===Restock=Item===//
                    case "3":
                    case "restock":
                        textmenu_AddStockToItem();
                        break;
                    //===Display=All=Items===//
                    case "4":
                    case "display":
                    case "all":
                        textmenu_DisplayAllItems();
                        break;
                    //===Search=For=Item===//  //Unimplemented
                    case "5":
                    case "search":
                    case "find":
                        textmenu_SearchItems();
                        break;
                    //===Unknown===//
                    default:
                        Console.WriteLine("Did not understand command '" + userinput_Raw + "', please try again (Press enter to continue)");
                        Console.ReadLine();
                        break;
                }
            }
        }

        //==============================//
        //=======textmenu_NewItem=======//
        //==============================//
        static void textmenu_NewItem()
        {
            //======Printing=Out======//
            Console.WriteLine("<======New=Item======>");
            Console.WriteLine("> This section demonstrates adding a new item to the system - it does not let you customize all attributes");
            Console.WriteLine("> Stock defaults to 5");
            Console.WriteLine();

            //======Name======//
            Console.Write("Enter New Item's Name: ");  //Prompt for input
            String userinput_Name = Console.ReadLine();  //Reading input
            Console.WriteLine();  //Spacing

            //======Price======//
            Console.Write("Enter New Item's Price (Will crash program if not entered properly): ");  //Prompt for input
            String userinput_PriceRaw = Console.ReadLine();  //Reading input
            double userinput_PriceTranslated = double.Parse(userinput_PriceRaw);  //Translating input
            Console.WriteLine();  //Spacing

            //======Description======//
            Console.Write("Enter New Item's Description: ");  //Prompt for input
            String userinput_Description = Console.ReadLine();  //Reading input
            Console.WriteLine();  //Spacing

            //======Creating======//
            InventoryItem newItem = new InventoryItem(userinput_Name, userinput_PriceTranslated, 5, 0, 0, userinput_Description);  //Creating new item
            inventory.addInventoryItem(newItem);  //Adding to item inventory
            Console.WriteLine("Item created...");  //Confirming creation
        }

        //=================================//
        //=======textmenu_RemoveItem=======//
        //=================================//
        static void textmenu_RemoveItem()
        {
            //======Printing=Out======//
            Console.WriteLine("<======Remove=Item======>");
            Console.WriteLine("Enter the name of the item you want to remove:");

            //======Receiving=Input======//
            String userinput = Console.ReadLine();
            List<InventoryItem> returnedList = inventory.searchForItem(userinput, 0);
            if (returnedList.Count == 0)
            {
                Console.WriteLine("No items found of that name");
            }
            else
            {
                Console.WriteLine("Deleting items...");
                for (int i = 0; i < returnedList.Count; i++)
                {
                    inventory.removeInventoryItem(returnedList[i]);

                }
            }
        }

        //======================================//
        //=======textmenu_AddStockToItem=======//
        //======================================//
        static void textmenu_AddStockToItem()
        {
            //======Printing=Out======//
            Console.WriteLine("<======Add=Stock=To=Item======>");
            Console.WriteLine("Enter the name of the item you want to add stock:");

            //======Receiving=Input======//
            String userinput = Console.ReadLine();
            List<InventoryItem> returnedList = inventory.searchForItem(userinput, 0);
            if (returnedList.Count == 0)
            {
                Console.WriteLine("No items found of that name");
            }
            else
            {
                Console.WriteLine("Enter the amount you want to add to the items stock (Will crash program if not entered properly): ");
                String userinput_RawStocksToAdd = Console.ReadLine();  //Reading input
                int userinput_StocksToAdd = int.Parse(userinput_RawStocksToAdd);  //Translating input
                for (int i = 0; i < returnedList.Count; i++)
                {
                    inventory.restockInventoryItem(returnedList[i], userinput_StocksToAdd);
                }
                Console.WriteLine("Restocking...");
                Console.WriteLine();
            }
        }

        //======================================//
        //=======textmenu_DisplayAllItems=======//
        //======================================//
        static void textmenu_DisplayAllItems()
        {
            //======Printing=Out======//
            Console.WriteLine("<======Display=Items======>");
            inventory.displayAllInventoryItems();  //Displaying all items
            Console.WriteLine();
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();
        }

        //==================================//
        //=======textmenu_SearchItems=======//
        //==================================//
        static void textmenu_SearchItems()
        {
            //======Printing=Out======//
            Console.WriteLine("<======Search=Items======>");
            Console.WriteLine("Enter the name of the item you wish to search:");

            //======Receiving=Input======//
            String userinput_Name = Console.ReadLine();
            List<InventoryItem> returnedList = inventory.searchForItem(userinput_Name, 0);
            if (returnedList.Count == 0)
            {
                Console.WriteLine("No items found of that name");
            }
            else
            {
                for (int currentItem = 0; currentItem < returnedList.Count; currentItem++)  //Looping through list of items
                {
                    InventoryItem ci = returnedList[currentItem];  //Defining current item for ease of use
                    Console.WriteLine("-> " + ci.getItemName() + " for $" + ci.getItemPrice() + " with " + ci.getItemStock() + " left in stock: " + ci.getItemDescription());  //Printing inventory item
                }
            }


            //======Printing=Out======//
            Console.WriteLine("Enter the name of the item you wish to search:");

            //======Receiving=Input======//
            String userinput_RawStock = Console.ReadLine();
            int userinput_Stock = int.Parse(userinput_RawStock);
            List<InventoryItem> returnedList1 = inventory.searchForItem(null, userinput_Stock);
            if (returnedList1.Count == 0)
            {
                Console.WriteLine("No items found of that name");
            }
            else
            {
                for (int currentItem = 0; currentItem < returnedList1.Count; currentItem++)  //Looping through list of items
                {
                    InventoryItem ci = returnedList1[currentItem];  //Defining current item for ease of use
                    Console.WriteLine("-> " + ci.getItemName() + " for $" + ci.getItemPrice() + " with " + ci.getItemStock() + " left in stock: " + ci.getItemDescription());  //Printing inventory item
                }
            }
            Console.WriteLine();
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();
        }
    }
}
